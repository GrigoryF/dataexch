﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public class Proxy : RequestBase
    {
        HttpReq Subject;
        public override async Task<string> Request(Dictionary<string, string> json)
        {
            if (Subject == null)
                Subject = new HttpReq();
            return await Subject.Request(json);
        }
    }
}
