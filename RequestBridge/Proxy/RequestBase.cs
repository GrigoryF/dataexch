﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public abstract class RequestBase
    {
        public abstract Task<string> Request(Dictionary<string, string> fields);
    }
}
