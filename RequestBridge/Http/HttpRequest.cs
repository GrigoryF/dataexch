﻿using RequestBridge.Properties;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using xNet;

namespace DataExchange
{
    public class HttpReq : RequestBase
    {
        public override async Task<string> Request(Dictionary<string, string> fields)
        {
            using (var request = new HttpRequest())
            {
                if (fields.Count() == 5)
                {
                    var @params = fields.Take(4);
                    foreach (var field in @params)
                        request.AddField(field.Key, field.Value);
                    request.AddFile(fields.Last().Key, fields.Last().Value);
                    request.Post(Settings.Default.serverUrl).None();
                    return request.Response.StatusCode.ToString();
                }
                else  //getstatuses
                {
                    var reqParams = new RequestParams();
                    foreach (var field in fields)
                        reqParams[field.Key] = field.Value;
                    var content = request.Post(Settings.Default.serverUrl, reqParams).ToString();
                    //request.Response.ToString();
                    return content; 
                }
                
                return null;
            }
        }
    }
}
