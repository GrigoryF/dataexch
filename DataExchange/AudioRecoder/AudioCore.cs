﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace DataExchange
{
    public class AudioCore
    {
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        public Dictionary<string, string> Run(string clientId)
        {
            mciSendString("open new Type waveaudio Alias recsound", "", 0, 0);
            mciSendString("record recsound", "", 0, 0);
            Console.WriteLine("recording, press Enter to stop and save ...");
            Console.ReadLine();
            mciSendString("save recsound audio.m4a", "", 0, 0);
            mciSendString("close recsound ", "", 0, 0);
            return CreateJson(clientId);
        }

        Dictionary<string, string> CreateJson(string clientId)
        {

            return JsonDataSender.GetJsonChanging("audio", "audio.m4a", clientId);
        }
    }
}
