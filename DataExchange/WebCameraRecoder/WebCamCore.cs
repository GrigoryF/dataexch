﻿using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataExchange
{
    public class WebCamCore
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideo = null;
        private VideoFileWriter FileWriter = new VideoFileWriter();
        Bitmap video;
        public Dictionary<string, string> Run(string clientId)
        {

            string devices = ""; 
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                devices +=  VideoCaptureDevice.Name;
            }

            if (string.IsNullOrEmpty(devices))
            {
                Console.WriteLine("\nno inputs");
                return null;
            }

            FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[0].MonikerString);
            FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
            
            FinalVideo.Start();
            FileWriter.Open("test.mp4", FinalVideo.VideoCapabilities[0].FrameSize.Width, FinalVideo.VideoCapabilities[0].FrameSize.Height, 25, VideoCodec.Default, 5000000);
            Thread.Sleep(20000);
            FinalVideo.Stop();
            FileWriter.Close();
            return CreateJson(clientId);
        }
        public void Save()
        { 
            FileWriter.WriteVideoFrame(video);
        }

        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            video = (Bitmap)eventArgs.Frame.Clone();
            Save();
        }

        Dictionary<string, string> CreateJson(string clientId)
        {
            return JsonDataSender.GetJsonChanging("webvideo", "test.mp4", clientId);

        }
    }
}
