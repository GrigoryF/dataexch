﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Text;
using System.Threading;

namespace DataExchange
{
    public class GeoCore
    {
        public Dictionary<string, string> Run(string clientId)
        {
            GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
            watcher.TryStart(false, TimeSpan.FromMilliseconds(1000));
            Thread.Sleep(2000);
            GeoCoordinate coord = watcher.Position.Location;
            var latLng = "[{\"lat\":\"" + coord.Latitude + "\",\"lng\":\"" + coord.Longitude + "\"}]";
            return JsonDataSender.GetJsonChanging("geo", latLng, clientId);
        }
    }
}
