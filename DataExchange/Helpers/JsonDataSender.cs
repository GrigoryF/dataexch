﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public class JsonDataSender
    {

        public static string GetJsonDisabling(string name, string clientId)
        {
            dynamic json = new JObject();
            switch (name)
            {
                case "audio":
                    json.disable_client_audio = "true";
                    break;
                case "webvideo":
                    json.disable_client_video_back = "true";
                    break;
                case "screen":
                    json.disable_client_screen = "true";
                    break;
                case "geo":
                    json.disable_client_photo_back = "true";
                    break;
                case "photo":
                    json.disable_client_photo_front = "true";
                    break;
            }
            json.data_put = "true";
            json.client_id = clientId;
            return json.ToString();
        }


        public static Dictionary<string, string> GetJsonChanging(string type, string fileName, string clientId)
        {
            var fields = new Dictionary<string, string>();

            switch (type)
            {
                case "audio":
                    fields.Add("change_client_audio", "true");
                    break;
                case "webvideo":
                    fields.Add("change_client_video_front", "true");
                    break;
                case "screen":
                    fields.Add("add_client_screen", "true");
                    break;
                case "geo":
                    fields.Add("change_client_geolocation", "true");
                    break;
                case "photo":
                    fields.Add("change_client_photo_front", "true");
                    break;
                case "screenvideo":
                    fields.Add("change_client_video_back", "true");
                    break;
            }
            fields.Add("client_id", clientId);
            fields.Add("data_put", "true");
            fields.Add("new", fileName);
            fields.Add("userfile", fileName);
            return fields;
        }

        public static string GetStatusJson(string clientid)
        {
            var fields = new Dictionary<string, string>();

            dynamic json = new JObject();
            json.data_put = "true";
            json.get_client_statuses = "true";
            json.client_id = clientid;
            return json.ToString();
        }

        public static Dictionary<string, string> GetStatusFields(string clientId)
        {
            var fields = new Dictionary<string, string>();
            fields.Add("data_put", "true");
            fields.Add("get_client_statuses", "true");
            fields.Add("client_id", clientId);
            return fields;
        }

        public static Dictionary<string, string> GetId(string clientHash)
        {
            var fields = new Dictionary<string, string>();
            fields.Add("data_put", "true");
            fields.Add("get_client_id", "true");
            fields.Add("client_hash", clientHash);
            fields.Add("client_type", "2");
            return fields;
        }

        public static Dictionary<string, string> GetDisableFields(string type, string clientId)
        {
            var fields = new Dictionary<string, string>();
            fields.Add("data_put", "true");
            fields.Add("client_id", clientId);
            switch (type)
            {
                case "audio":
                    fields.Add("disable_client_audio", "true");
                    break;
                case "webvideo":
                    fields.Add("disable_client_video_front", "true");
                    break;
                case "screen":
                    fields.Add("disable_client_screen", "true");
                    break;
                case "geo":
                    fields.Add("disable_client_photo_back", "true");
                    break;
                case "photo":
                    fields.Add("disable_client_photo_front", "true");
                    break;
                case "screenvideo":
                    fields.Add("disable_client_video_back", "true");
                    break;
            }
            return fields;
        }

    }
}

