﻿using Accord.Video;
using Accord.Video.FFMPEG;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;

namespace DataExchange
{
    public class MonitorCore
    {
        VideoFileWriter FileWriter = new VideoFileWriter();
        ScreenCaptureStream stream;
        Bitmap bitmap;
        public Dictionary<string, string> Run(string clientId)
        {
            Rectangle screenArea = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            stream = new ScreenCaptureStream(screenArea);
            stream.NewFrame += new NewFrameEventHandler(video_NewFrame);
            FileWriter.Open("testmonitor.mp4", screenArea.Width, screenArea.Height, 25, VideoCodec.MPEG4, 5000000);
            stream.Start();
            Thread.Sleep(10000);
            stream.SignalToStop();
            stream.Stop();
            FileWriter.Close();
            return CreateJson(clientId);
        }

        public void Save()
        {
            FileWriter.WriteVideoFrame(bitmap);
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            bitmap = (Bitmap)eventArgs.Frame.Clone();
            Save();
        }

        Dictionary<string, string> CreateJson(string clientId)
        {
            return JsonDataSender.GetJsonChanging("webvideo", "testmonitor.mp4", clientId);
        }
    }
}
