﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public class ScreenCore
    {
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        public Dictionary<string, string> Run(string clientId)
        {
            Graphics graph = null;
            Rectangle screenArea = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            var bmp = new Bitmap(screenArea.Width, screenArea.Height);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            graph = Graphics.FromImage(bmp);

            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            graph.CopyFromScreen(0, 0, 0, 0, bmp.Size);

            bmp.Save("screen.jpg", jpgEncoder, myEncoderParameters);
            
            return CreateJson(clientId);
        }


        public Dictionary<string, string> CreateJson(string client_id)
        {
            return JsonDataSender.GetJsonChanging("screen", "screen.jpg", client_id);
        }

        byte[] Convert()
        {
            var r = File.ReadAllBytes("screen.jpg");
            return r;
        }

    }
}
