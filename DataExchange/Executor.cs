﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public class Executor
    {
        internal static void Run() //start execution loop (main thread)
        {
            BlockingCollection<Action>[] queues = new BlockingCollection<Action>[1];
            queues[0] = Queues.admf_queue;
            while (true)
            {
                Action action;
                int index = BlockingCollection<Action>.TakeFromAny(queues, out action);
                try
                {
                    action();
                }
                catch (Exception ex)
                {
                    //logger.Fatal(ex.ToString());
                }
            }
        }
    }
}
