﻿using System;
using System.IO;

namespace DataExchange
{
    public class FolderCore
    {
        public void Run()
        {
            foreach (DriveInfo label in DriveInfo.GetDrives())
            {
                Console.WriteLine(label+":\n");
                string[] fileEntries = Directory.GetFiles(label.Name);
                string[] directoriesEntries = Directory.GetDirectories(label.Name);
                foreach (string fileName in directoriesEntries)
                {
                    Console.WriteLine(fileName + "\n");
                }
            }
        }
    }
}
