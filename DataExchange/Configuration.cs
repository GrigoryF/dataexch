﻿using DataExchange.Properties;

namespace DataExchange
{
    public class Configuration
    {
        public string ServerUrlHttp { get; set; } = Settings.Default.serverUrl;
    }
}
