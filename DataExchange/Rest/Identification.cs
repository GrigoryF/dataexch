﻿using DataExchange.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DataExchange
{
    public class Identification
    {
        public static string GetUniqueHardwaeId()
        {
            StringBuilder sb = new StringBuilder();

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                  "SELECT * FROM Win32_Processor");

            foreach (ManagementObject queryObj in searcher.Get())
            {
                sb.Append(queryObj["NumberOfCores"]);
                sb.Append(queryObj["ProcessorId"]);
                sb.Append(queryObj["Name"]);
                sb.Append(queryObj["SocketDesignation"]);

                Console.WriteLine(queryObj["ProcessorId"]);
                Console.WriteLine(queryObj["Name"]);
                Console.WriteLine(queryObj["SocketDesignation"]);
            }

            searcher = new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject queryObj in searcher.Get())
            {
                sb.Append(queryObj["Manufacturer"]);
                sb.Append(queryObj["Name"]);
                sb.Append(queryObj["Version"]);

                Console.WriteLine(queryObj["Manufacturer"]);
                Console.WriteLine(queryObj["Name"]);
                Console.WriteLine(queryObj["Version"]);
            }

            searcher = new ManagementObjectSearcher("root\\CIMV2",
                   "SELECT * FROM Win32_BaseBoard");

            foreach (ManagementObject queryObj in searcher.Get())
            {
                sb.Append(queryObj["Product"]);
                Console.WriteLine(queryObj["Product"]);
            }

            var bytes = Encoding.ASCII.GetBytes(sb.ToString());
            SHA256Managed sha = new SHA256Managed();

            byte[] hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash);
        }



        private static Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
        public static string CreateId(int size = 10)
        { 
            StringBuilder builder = new StringBuilder();

            var currentInstance = Registry.CurrentUser;
            RegistryKey currentUserKey = null;
            var subNames = currentInstance.GetSubKeyNames();
            if (!subNames.Contains("dataexchangeid"))
            {
                char ch;
                for (int i = 0; i < size; i++)
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    builder.Append(ch);
                }


                currentUserKey = currentInstance.CreateSubKey("dataexchangeid");
                Settings.Default.InstanceId = Encoder.Encrypt(builder.ToString(), "test");
                currentUserKey.SetValue("dataexchangeid", Settings.Default.InstanceId);
                Settings.Default.Save();
            }
            else
            {
                Settings.Default.InstanceId = currentInstance.OpenSubKey("dataexchangeid", true).GetValue("dataexchangeid").ToString();
                Settings.Default.Save();
                builder.Append(Decoder.Decrypt(Settings.Default.InstanceId, "test"));
            }
            return builder.ToString();
        }
    }
}
